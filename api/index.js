'use strict'
const express = require("express");
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require("../config.json");
const app = express();


//Importamos los controladores de la api
const contactoRouter = require('./core/routes/contacto');

//configure app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set('views', './core/views')
app.set('view engine', 'pug');



app.use("/api/v1/contacto", contactoRouter);



module.exports = app;