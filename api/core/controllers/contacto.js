const SendgridHelper = require("../helpers/SendgridHelper");

const CuentaController = {
    sendContacto: function(data){
        return SendgridHelper.send(data);
    }
}

module.exports = CuentaController;