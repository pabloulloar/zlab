const express = require('express');
const router = express.Router();

const ContactoController = require("../controllers/contacto");

router.post("/",(req,res,next) =>{
    req.body = mongoSanitize(req.body);
    var data = {
        to: req.body.to,
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        message: req.body.message
    }

    ContactoController.sendContacto(data)
    .then(resp => {
        res.status(200).json(resp);
        next();
    })
    .catch(e => {
        res.status(500).json({code: 500,message: e.message});
        next();
    });
});



module.exports = router;