const express = require('express');
const app = express();
const api = require("./api");
const config = require("./config");

var moment = require('moment');
moment.locale('es');

app.use('/', express.static(__dirname +  '/public/'));
app.use(api);

const server = app.listen(config.service.port, config.service.host, () => {
  console.log(`Server running at http://${config.service.host}:${config.service.port}/`);  
});
